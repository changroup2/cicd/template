package com.example.demo.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Slf4j
@Controller
@RequestMapping("/test")
public class TestController {

    @Value("${dbdatabase}")
    private String db_database;

    @RequestMapping("/test")
    @ResponseBody
    public String test(){
        log.debug("[test] db[{}]",db_database);
        return "asdf1";

    }
}
